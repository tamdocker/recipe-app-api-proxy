#recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment variables

* 'listen_port' - Port to listen on (default: '8000')
* 'App_host' - Hostname of the app to forward requests to (default: 'app')
* 'App_Port' - Port of the app to forward requests to (default: '9000')